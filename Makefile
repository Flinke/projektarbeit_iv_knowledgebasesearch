CC1=pdflatex

PDF=PDFName.pdf

${PDF} : main.tex *.tex literatur.bib glossdata0.txt
	#rm -vf *.glg *.glo *.gls *.ist *.acn *.acr *.alg #Löscht Alle glossaries Metadateien
	${CC1} $<
	makeglossaries $(subst .tex,.glo,$<)
	makeglossaries $(subst .tex,.acn,$<)
	${CC1} $<
	makeglossaries $(subst .tex,.glo,$<)
	makeglossaries $(subst .tex,.acn,$<)
	biber $(subst .tex,.bcf,$<)
	${CC1} $<
	${CC1} $<
	${CC1} $<
#	cp -v ./$(subst .tex,.pdf,$<) ./FertigePDF/$@
	if [ -d ./FertigePDF/ ]; then echo "exist"; else mkdir ./FertigePDF; fi
	mv -v ./$(subst .tex,.pdf,$<) ./FertigePDF/$@
	touch -r ./FertigePDF/$@ ./$@

clean :
	rm -vf *.dvi *.pdf *.ps *.aux *.lof *.toc *.lot *.log *.bbl *.bcf *.blg *.glg *.glo *.gls *.ist *.acn *.acr *.alg *.run.xml

