\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\contentsline {section}{\nonumberline Abbildungsverzeichnis}{II}% 
\contentsline {section}{\numberline {1}Einleitung}{1}% 
\contentsline {subsection}{\numberline {1.1}Motivation}{1}% 
\contentsline {subsection}{\numberline {1.2}Zielstellung}{2}% 
\contentsline {subsection}{\numberline {1.3}Aufbau der Arbeit}{3}% 
\contentsline {section}{\numberline {2}Anforderungen}{5}% 
\contentsline {subsection}{\numberline {2.1}Rollen}{6}% 
\contentsline {subparagraph}{\nonumberline Probleme und Herausforderungen:}{6}% 
\contentsline {subparagraph}{\nonumberline Ziele:}{6}% 
\contentsline {subparagraph}{\nonumberline Probleme und Herausforderungen:}{6}% 
\contentsline {subparagraph}{\nonumberline Ziele:}{7}% 
\contentsline {subparagraph}{\nonumberline Probleme und Herausforderungen:}{7}% 
\contentsline {subparagraph}{\nonumberline Ziele:}{7}% 
\contentsline {subparagraph}{\nonumberline Probleme und Herausforderugen:}{8}% 
\contentsline {subparagraph}{\nonumberline Ziele:}{8}% 
\contentsline {subparagraph}{\nonumberline Probleme und Herausforderungen:}{8}% 
\contentsline {subparagraph}{\nonumberline Ziele:}{8}% 
\contentsline {subparagraph}{\nonumberline Probleme und Herausforderungen:}{9}% 
\contentsline {subparagraph}{\nonumberline Ziele:}{9}% 
\contentsline {subparagraph}{\nonumberline Probleme und Herausforderungen:}{9}% 
\contentsline {subparagraph}{\nonumberline Ziele:}{10}% 
\contentsline {subsection}{\numberline {2.2}User Stories}{10}% 
\contentsline {section}{\numberline {3}Konzeption}{13}% 
\contentsline {subsection}{\numberline {3.1}Aufbau}{13}% 
\contentsline {subsection}{\numberline {3.2}Schnittstellen}{14}% 
\contentsline {subsection}{\numberline {3.3}Entity-Relationship-Modell}{19}% 
\contentsline {subsection}{\numberline {3.4}Apache Solr}{21}% 
\contentsline {section}{\numberline {4}Fazit}{23}% 
\contentsline {section}{Literatur- und Quellenverzeichnis}{V}% 
\contentsline {section}{Ehrenw\IeC {\"o}rtliche Erkl\IeC {\"a}rung}{VI}% 
